package org.launchcode.training;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.launchcode.training.data.CarMemoryRepository;
import org.launchcode.training.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CarControllerTests {

    @Autowired
    private MockMvc mockMvc;

    private CarMemoryRepository carRepository = new CarMemoryRepository();



    @Test
    public void testCanViewListOfCars() throws Exception {
        carRepository.save(new Car("Toyota", "Camry", 10, 30));
        mockMvc.perform(get("/car"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Camry")));

    }

    //TODO: implement test for going to route /car/:id
    @Test
    public void testCallCarById() throws Exception {
        Car testCar = new Car("Toyota", "Camry", 10, 30);
        carRepository.save(testCar);
        mockMvc.perform(get("/car/" + testCar.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Toyota")))
                .andExpect(content().string(containsString("Camry")));
    }


}
